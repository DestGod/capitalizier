export function capitalizier(string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
}